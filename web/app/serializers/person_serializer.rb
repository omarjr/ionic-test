class PersonSerializer < ActiveModel::Serializer
  attributes :name, :age, :email
end
