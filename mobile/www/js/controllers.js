angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('PersonCtrl', function($scope, $http) {
  $http.get('https://fga.unb.br/api/v1/people?limit=10000&private_token=e7ee78d8a87bcef06e1fd84cc6874098').then(function(resp){
    for (var key in resp) {
      console.log(resp);
    }

    $scope.people = resp.data.people;
  });
})

// .controller('ArticlesCtrl', function($scope, $http) {
//   $http.get('https://fga.unb.br/api/v1/communities/15/articles?limit=100&private_token=e7ee78d8a87bcef06e1fd84cc6874098').then(function(resp){

//       console.log(resp);


//     $scope.articles = resp.data.articles;
//   });
// })

// .controller('PersonCtrl', function($scope, Person) {
//   Person.query().$promise.then(function(response){
//     $scope.people = response;
//   });
// })


.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
